=begin
class Veiculo
    #Métodos criados implicitamente pelo attr_accessor :cor
    #Leitura
    def cor
        return @cor
    end

    #Escrita
    def cor=(nome_da_cor)
        @cor = nome_da_cor
    end

    #Variavel de classe
    @@quantidade_de_veiculos = 5

    def initialize(cor)
        @@quantidade_de_veiculos += 1
        @cor = cor
    end

    def self.getQuantidade
        return @@quantidade_de_veiculos
    end

end


veiculo = Veiculo.new("Branco")
veiculo2 = Veiculo.new("Azul")
veiculo3 = Veiculo.new("Preto")
veiculo4 = Veiculo.new("Vermelho")
veiculo.cor = "Azul"
#puts veiculo.cor

puts Veiculo.getQuantidade
puts veiculo4.getQuantidade
=end



