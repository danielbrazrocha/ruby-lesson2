#Uso de módulo direto no objeto.

module Teste
    def diz_ola
       puts "Olá."
    end
end	
  
class Pessoa
end
  
pessoa = Pessoa.new
pessoa2 = Pessoa.new
pessoa2.extend(Teste)
  
puts pessoa2.diz_ola