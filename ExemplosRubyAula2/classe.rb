
class Veiculo
    attr_accessor :id, :passageiros, :motorista, :cor, :peso, :altura, :numero_rodas, :velocidade, :ligado, :montadora
    
    @@incrementador_id = 0

    def initialize(cor:, peso:, altura:, numero_rodas:, montadora:)
        @id = @@incrementador_id
        @passageiros = []
        @motorista = ""
        @cor = cor
        @peso = peso
        @altura = altura
        @numero_rodas = numero_rodas
        @velocidade = 0
        @ligado = false
        @montadora = montadora

        @@incrementador_id += 1
    end

    def ligar
        @ligado = true
        puts "Ligando o Veículo."
    end

    def desligar
        @ligado = false
        puts "Desligando o Veículo"
    end

    def acelerar(valor)
        @velocidade = @velocidade + valor
        puts "Acelerando o Veículo"
    end

    def frear(valor)
        @velocidade = @velocidade - valor
        puts "Freando o Veículo"
    end
end

#class Classe < SuperClasse
class Carro < Veiculo
    attr_accessor :tipo

    def initialize(cor:, peso: , altura:, numero_rodas:, montadora:, tipo:)
        super(cor: cor, peso: peso, altura: altura, numero_rodas: numero_rodas, montadora: montadora)
        @tipo = tipo
    end
end

class Aviao < Veiculo
    attr_accessor :tamanho_asa

    def initialize(cor:, peso:, altura:, numero_rodas:, montadora:, tamanho_asa:)
        super(cor: cor, peso: peso, altura: altura, numero_rodas: numero_rodas, montadora: montadora)
        @tamanho_asa = tamanho_asa
    end

    def voar(valor)
        puts "Voando..."
        @velocidade += valor
    end

    def levantar_trem_de_pouso
        puts "Levantando trem de pouso"
    end

    def abaixar_trem_de_pouso
        puts "Abaixando trem de pouso"
    end
end

class Lancha < Veiculo
    def initialize(cor:, peso:, altura:, numero_rodas:, montadora:)
        super
    end

    def jogar_ancora 
        puts "Âncora Jogada com Sucesso!"
    end
end


#Veículo
=begin
veiculo = Veiculo.new(cor: "Branco", peso: 300, altura: 2.5, numero_rodas: 4, montadora: "BMW")
puts veiculo.velocidade
veiculo.ligar
veiculo.acelerar(5)
puts veiculo.velocidade
veiculo.desligar
=end


#Carro

=begin
carro = Carro.new(cor: "Prata", peso: 250, altura: 2, numero_rodas: 4, montadora: "Honda", tipo: "Passeio")
puts carro.velocidade
carro.acelerar(2)
carro.acelerar(3)
carro.frear(1)
puts carro.velocidade
=end



#Avião
=begin
aviao = Aviao.new(cor: "Branco", peso: 12.5, altura: 13.5, numero_rodas: 4, montadora: "BMW", tamanho_asa: 15)
aviao.ligar
aviao.voar(10)
aviao.desligar
puts aviao.velocidade
=end




#Lancha
=begin
lancha = Lancha.new(cor: "Azul", peso: 12.5, altura: 13.5, numero_rodas: 0, montadora: "BMW")
lancha.ligar
lancha.jogar_ancora
lancha.desligar
puts lancha.ligado
=end



lancha2 = Lancha.new(cor: "Azul", peso: 12.5, altura: 13.5, numero_rodas: 0, montadora: "BMW")

veiculo = Veiculo.new(cor: "Branco", peso: 300, altura: 2.5, numero_rodas: 4, montadora: "BMW")

carro = Carro.new(cor: "Prata", peso: 250, altura: 2, numero_rodas: 4, montadora: "Honda", tipo: "Passeio")

aviao = Aviao.new(cor: "Branco", peso: 12.5, altura: 13.5, numero_rodas: 4, montadora: "BMW", tamanho_asa: 15)

lancha = Lancha.new(cor: "Azul", peso: 12.5, altura: 13.5, numero_rodas: 0, montadora: "BMW")


print "id do veiculo: #{veiculo.id}\n\n"
    
print "id do carro: #{carro.id}\n\n"

print "id do aviao: #{aviao.id}\n\n"

print "id do lancha: #{lancha.id}\n\n"

print "id do lancha2: #{lancha2.id}\n\n"
