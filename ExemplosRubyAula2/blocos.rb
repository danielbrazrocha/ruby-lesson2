=begin
def meu_metodo()
    puts "Antes do bloco"
    yield
    puts "Depois do bloco"
end

meu_metodo do 
    puts "Dentro do bloco"
end
=end



=begin
def teste
    parametro1 = "a"
    parametro2 = "b"
    yield parametro1, parametro2
end

teste do |param1, param2|
    puts "Os parâmetros recebidos pelo bloco são: #{param1} e #{param2}"
end
=end



#Mostrando chaves no blcoco


=begin
def meu_metodo(&meu_bloco)
    puts "Antes do bloco"
    yield
    puts "Depois do bloco"
end

meu_metodo { 
    puts "Dentro do bloco"
}
=end



=begin
array = [5, 6, 7, 8]

array.each do |numero|
    puts numero
end
=end