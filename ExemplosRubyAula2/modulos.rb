#Implementação de Módulo Padrão


module Matematica
    PI = 3.14

    class Calculo
        def self.teste
            puts "teste"
        end
    end

    def Matematica.soma(a, b)
        return a + b
    end

    def Matematica.subtracao(a, b)
        return a - b
    end

    def Matematica.multiplicacao(a, b)
        return a * b
    end

    def Matematica.divisao(a, b)
        return a.to_f / b.to_f
    end
end

#puts Matematica::soma(1, 2)
#puts Matematica::subtracao(1, 2)
#puts Matematica::multiplicacao(1, 2)
#puts Matematica::divisao(1, 2)
#puts Matematica::PI

puts Matematica::Calculo.teste






#Implementação de Módulo sendo incluido na classe

=begin
module Pessoa
    def nome=(nome)
        @nome = nome
    end

    def nome
        return @nome
    end
end

class Crianca
    include Pessoa
end

crianca = Crianca.new
crianca.nome="Criança 1"

crianca2 = Crianca.new
crianca2.nome = "Criança 2"

puts crianca.nome
puts crianca2.nome
=end





#Módulo em arquivo externo

=begin
require './modulo_separado'

class Pessoa
    include HabilidadeFalar
end

pessoa = Pessoa.new
pessoa.diga_oi
pessoa.diga_tchau
=end