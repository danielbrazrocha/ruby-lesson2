class Professor
    #definindo setters e getters para possibilitar leitura e escrita destes atributos no objeto 
    attr_accessor :id, :matricula, :salario, :materias
    
    
    @@quantidade_professores = 0

    def initialize(matricula:, :salario)
        @id = @@quantidade_professores
        @matricula = matricula
        @salario = salario
        @materias = []
        
        @@quantidade_professores += 1
    end

    #definindo função para adicionar matérias
    def adicionar_materia(nome_materia)
        @materias += nome_materia
        puts "Adicionada uma matéria"
    end
end

class Materia
    #definindo setters e getters para possibilitar leitura e escrita destes atributos no objeto 
    attr_accessor :ementa, :nome, :professores

    @@quantidade_materias = 0
    def initialize(ementa:, nome:)
        @id = @@quantidade_materias
        @ementa = ementa
        @nome = nome
        @professores = []

        @@quantidade_materias += 1
    end
    
    #definindo função para adicionar professores
    def adicionar_professor(nome_professor)
        @professores += nome_professor
        puts "Professor adicionado a matéria."
    end
end


class Usuario
    attr_accessor :email, :senha, :nome, :logado, :dataNascimento
    
    @@numeroUsuarios = 0

    def initialize(email:, senha:, nome:, dataNascimento:)
        @id = @@numeroUsuarios
        @email = email
        @senha = senha
        @nome = nome
        @dataNascimento = dataNascimento
        @logado = false
        
        @@numeroUsuarios += 1
    end

    def idade 
        anoAtual = 2020
        idade = anoAtual - @dataNascimento
        puts "The user’s age is " + idade + " years"
    end

    def logar(senha)
        if (senha == @senha)
            @logado = true
            puts "Você está conectado!"
        end
    end
    
    def deslogar 
        @logado = false
        puts "Você se desconectou com sucesso!"
    end

end


class Aluno
    attr_accessor :turma, :matricula, :periodoLetivo, :curso

    @@quantidadeAlunos = 0
    def initialize(matricula:, periodoLetivo:, curso:)
        @id = @@quantidadeAlunos
        @turma = []
        @matricula = matricula
        @periodoLetivo = periodoLetivo
        @curso = curso  

        @@quantidadeAlunos += 1
    end

    def inscrever(nome_da_turma)
        @turma += nome_da_turma
        puts "Você foi inscrito com sucesso."
    end
end

class Turma
    attr_accessor :nome, :horario, :diaSemana, :inscricaoAberta, :alunos

    @@quantidadeTurmas = 0

    def initialize(nome:, horario:, diaSemana:)
        @id = @@quantidadeTurmas
        @nome = nome
        @horario = horario
        @diaSemana = diaSemana
        @inscricaoAberta = false
        @alunos = []

        @@quantidadeTurmas += 1
    end

    def abrir_inscricao
        @inscricaoAberta = true
        puts "As inscrições estão abertas!"
    end

    def fechar_inscricao
        @inscricaoAberta = false
        puts "As incrições estão encerradas."
    end

    def adicionar_aluno(nome_aluno)
        @alunos += nome_aluno
        puts "O aluno foi adicionado com sucesso."
    end

end